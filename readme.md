# SBFormApp1

## About

SmallBasic forms app using Microsoft SmallBasic and LitDev Extensions

![SBFormApp1.png](./SBFormApp1.png?raw=true "Screenshot")

## SmallBasic

Download SmallBasic here:
https://smallbasic-publicwebsite.azurewebsites.net/

## LitDev Extensions

Download LitDev Extensions here:
https://github.com/litdev1/LitDev

## History

0.4:
- Fix hang in app caused by non re-entrant event handler being flooded with OnResize events. 
Issue:
https://github.com/litdev1/LitDev/issues/37
Reference:
https://social.technet.microsoft.com/wiki/contents/articles/22264.small-basic-threading.aspx

0.3:
- add Font button
- add some Tooltips
- fix app description in About dialog
- added resizing logic; works for making larger that initial size (see Issues).

0.2:
- command-line arguments captured in array
- add About dialog
- use Folder dialog for Preferences example
- fix missing Props/Prefs icons
- replace icon set with inconsistent sizes

0.1:
- initial release
- menus that update a status textbox
- fields saved/opened to/from settings file

## Issues

- Resizing form smaller than initial size causes app, and Windows, to hang (at least as run within VirtualBox).
- Client area background color does not paint new areas when resizing

## Contact

Stephen J Sepan
sjsepan@yahoo.com
9-17-2023
